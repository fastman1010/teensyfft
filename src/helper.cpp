/*
 * helper.cpp
 *
 *  Created on: 25.03.2019
 */

#include <Arduino.h>
#include <arm_math.h>
#include <helper.h>

IntervalTimer ledTimer;

/**
 * A function that blinks the built-in LED.
 */
void keepAlive() {
	digitalWriteFast(LED_BUILTIN, !digitalReadFast(LED_BUILTIN));
}

/**
 * Halt system in case of an error.
 */
void systemError(char *msg) {
	Serial.printf("System error: %s\n", msg);
	ledTimer.update(200000);
	while (1);
}

/**
 * Generates a digitized sine wave.
 * wave = buffer to store the generated signal
 * freq = desired signal frequency
 * rate = sample rate (samples per second)
 * samps = number of samples
 */
void generateSineWave(float *wave, uint16_t freq, uint16_t rate, uint16_t samps) {
	for (uint16_t i = 0; i < samps; i++) {
		wave[i * 2] = arm_sin_f32(2 * M_PI * freq * i / rate);		// real
		wave[i * 2 + 1] = 0;										// imag
	}
}


/**
 * Split string into separate words.
 */
std::vector<String> splitString(String cmd, uint8_t delimiter) {
	std::vector<String> cmdVec;
	splitString(&cmdVec, cmd, delimiter);
	return cmdVec;
}

void splitString(std::vector<String> *cmdVec, String cmd, uint8_t delimiter) {
	int8_t delimPos;
	delimPos = cmd.indexOf(delimiter);

	if (delimPos < 0) {
		cmdVec->push_back(cmd);
	} else {
		cmdVec->push_back(cmd.substring(0, delimPos));
		splitString(cmdVec, cmd.substring(delimPos + 1), delimiter);
	}
}
