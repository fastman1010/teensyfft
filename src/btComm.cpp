/*
 * btComm.cpp
 *
 *  Created on: 22.04.2019
 */

#include <Arduino.h>
#include <btComm.h>

BTComm::BTComm() {
}

void BTComm::init() {
	pinMode(BT_PIN_STATE, INPUT);
	BT_SER.begin(9600);
}

bool BTComm::isConnected() {
	return digitalRead(BT_PIN_STATE);
}

String BTComm::readCommand() {
	String cmd;
	if (BT_SER.available()) {
		cmd = BT_SER.readStringUntil('\n');
		if (cmd[cmd.length() - 1] == '\r') cmd = cmd.substring(0, cmd.length() - 1);
	} else {
		cmd = "";
	}
	return cmd;
}

void BTComm::writeString(String data) {
	BT_SER.print(data);
}

void BTComm::end() {
	BT_SER.print("AT+DISC\r\n");
	BT_SER.end();
}

BTComm::~BTComm() {
	end();
}

