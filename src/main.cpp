/*
 * main.cpp
 *
 *  Created on: 14.03.2019
 */

#include <adc_dma.h>
#include <arrayFunc.h>
#include <vector>
#include <Arduino.h>
#include <btComm.h>
#include <helper.h>
#include <IntervalTimer.h>
#include <process.h>
#include <Wire.h>
#include <WString.h>
#include <ym3812.h>
#include "stats.h"

#define PIN_ADC 16

float32_t sampBuffer[FFT_SHIFTS][FFT_SHIFT_LEN];
float32_t window[FFT_LEN];
uint8_t bufCurrent;
arm_cfft_radix4_instance_f32 fftInst;
YM3812 ym;
BTComm bt;

int main() {
	String cmdIn;

	std::vector<String> cmd;

	// Start PWM
	analogWriteFrequency(PWM_PIN, PWM_FREQ);
	analogWriteResolution(PWM_RES);
	analogWrite(PWM_PIN, (1 << (PWM_RES - 1)));

	ym.init();

	// Initialize buffers
	bufferInit(sampBuffer);
	bufCurrent = 0;

	// Initialize window
	initWindow(window, FFT_LEN);

	// Set up built-in LED
	pinMode(LED_BUILTIN, OUTPUT);
	ledTimer.begin(keepAlive, 1000000);
	Serial.begin(115200);

	// Blink more often!
	ledTimer.update(500000);
	// Initialize ADC with DMA
	adc_dma digitizer(PIN_ADC);

	// Initialize FFT instance
	if (arm_cfft_radix4_init_f32(&fftInst, FFT_LEN, 0, 1) != ARM_MATH_SUCCESS) {
		systemError((char*) "Could not initialize FFT instance.\n");
	}

	Serial.clear();
	bt.init();

	// Main loop
	while (1) {
		if (bt.isConnected()) {
			cmdIn = bt.readCommand();
			if (cmdIn != "") {
				cmd = splitString(cmdIn, ' ');
				bt.writeString(ym.processCommand(cmd));
			}
		}
		// Wait for something on serial input; reset MCU on any input
//		if (Serial.available()) {
//			break;
//		}
	}

	bt.end();
	SCB_AIRCR = 0x05FA0004; // Soft reset
}


/**
 * The main work is here! Process input when a buffer is filled.
 */
void processADCInput(uint16_t *adcBuffer) {
//	uint32_t timeStart = micros();
	uint16_t i;
	uint8_t freqsUsed;
	float32_t sampReal[FFT_LEN];
	float32_t specCompl[FFT_LEN * 2];
	float32_t specAbs[FFT_LEN / 2];
	static channelState synthStates[CHANNELS];
	static channelState freqStates[CHANNELS];

	for (i = 0; i < FFT_SHIFT_LEN; i++) {
		sampBuffer[bufCurrent][i] = (float32_t) adcBuffer[i];
	}
	bufCurrent = (bufCurrent + 1) % FFT_SHIFTS;

	bufferCopyPart(sampReal, sampBuffer, bufCurrent);

	// Do real work!
	arm_mult_f32(sampReal, window, sampReal, FFT_LEN);
	padRealToCompl(sampReal, specCompl, FFT_LEN);
	arm_cfft_radix4_f32(&fftInst, specCompl);
	arm_cmplx_mag_f32(specCompl, specAbs, FFT_LEN / 2);
	arm_scale_f32(specAbs, (1.0 / (FFT_LEN * 65535)), specAbs, FFT_LEN / 2); // normalize results

	if (ym.prepareSpectrum) printSpectrum(&fftInst, specAbs);
	// Find maximum values
	freqsUsed = findMaxValues(specAbs, specCompl, ym.frequencyCount, freqStates);
	if (ym.prepareSpectrum) {
		printFreqs(freqStates, freqsUsed);
		ym.prepareSpectrum = false;
	}

	playFrequencies(&ym, freqsUsed, freqStates, synthStates);
//	BT_SER.printf("%d\r\n", micros() - timeStart);
}
