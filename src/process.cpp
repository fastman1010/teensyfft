/*
 * process.cpp
 *
 *  Created on: 12.04.2019
 */

#include <arm_math.h>
#include <helper.h>
#include <ym3812.h>

#define SQRT6	2.44948974
#define SQRT2_3	0.81649658

float32_t amplMin = 0.00150;
float32_t amplMax = 0.05000;

/**
 * tau(x) for Quinn's Second Estimator.
 */
float32_t tau(float32_t x) {
	return (logf((3 * powf(x, 2)) + 6 * x + 1) / 4 - (SQRT6/24) * logf((x + 1 - SQRT2_3) / (x + 1 + SQRT2_3)));
}

/**
 * Find maximum values within single-sided spectrum.
 */
uint32_t findMaxValues(float32_t *specAbs, float32_t *specCompl, uint8_t numFreqs, channelState *freqStates) {
	bool freqExists;
	float32_t maxValue, kAppr, freqAppr, amplAppr, ap, dp, am, dm, d;
	uint8_t i, j;
	uint32_t k, x0, x1; // location of peak
	uint32_t freqsUsed = 0;

	for (i = 0; i < ((float32_t) MIN_FREQ / SAMP_RATE * FFT_LEN); i++) {
		specAbs[i] = 0; // cut off DC and low frequencies
	}

	for (i = 0; i < numFreqs; i++) {
		freqExists = false;
		arm_max_f32(specAbs, FFT_LEN / 2, &maxValue, &k); // find maximum
		if (maxValue > amplMin) { // compare with threshold

			// https://dspguru.com/dsp/howtos/how-to-interpolate-fft-peak/
			// Interpolate frequency using Quinn’s Second Estimator
			ap = (specCompl[(k + 1) * 2] * specCompl[k * 2] + specCompl[(k + 1) * 2 + 1] * specCompl[k * 2 + 1]) /
					(powf(specCompl[k * 2], 2) + powf(specCompl[k * 2 + 1], 2));
			dp = -ap / (1 - ap);
			am = (specCompl[(k - 1) * 2] * specCompl[k * 2] + specCompl[(k - 1) * 2 + 1] * specCompl[k * 2 + 1]) /
					(powf(specCompl[k * 2], 2) + powf(specCompl[k * 2 + 1], 2));
			dm = am / (1 - am);
			d = (dp + dm) / 2 + tau(dp * dp) - tau(dm * dm);
			kAppr = (float32_t) k + d;
			freqAppr = kAppr * ((float) SAMP_RATE / FFT_LEN);


			for (j = 0; j < freqsUsed; j++) { // Avoid repeating (or adjacent) frequencies
				if (freqAppr >= (1.0 - MAX_DEV) * freqStates[j].freq && freqAppr <= (1.0 + MAX_DEV) * freqStates[j].freq) {
					freqExists = true;
					break;
				}
			}

			if (!freqExists) {
				// Linear interpolation for amplitude
				x0 = floor(kAppr);
				x1 = ceil(kAppr);
				amplAppr = specAbs[x0] + (kAppr - x0) * ((specAbs[x1] - specAbs[x0]) / (x1 - x0));
				freqStates[freqsUsed].ampl = amplAppr;
				freqStates[freqsUsed].freq = freqAppr;
				freqStates[freqsUsed++].active = false;
			}

			// Omit value in the spectrum, avoid excessive maxima
			specAbs[k] = 0;
			specAbs[k - 1] *= 0.5;
			specAbs[k + 1] *= 0.5;

		} else { // maximum value does not exceed threshold, nothing to do here
			break;
		}
	}

	return freqsUsed;
}


/**
 * Assign frequencies to chip voices.
 */
void playFrequencies(YM3812 *ym, uint8_t freqsUsed, channelState *freqStates, channelState *synthStates) {
	uint8_t i, j;
	// Search through all available channels. If a frequency is already being played, keep it.
	for (i = 0; i < CHANNELS; i++) {
		if (synthStates[i].active == true) { // channel is in use
			synthStates[i].active = false; // switches to true if frequency matches
			for (j = 0; j < freqsUsed; j++) {
				// Ignore small frequency changes to avoid accessing the chip too often.
				if ((freqStates[j].freq >= synthStates[i].freq * (1.0 - MAX_DEV)) &&
						(freqStates[j].freq <= synthStates[i].freq * (1.0 + MAX_DEV))) {
					synthStates[i].active = true; // mark channel as already used
					freqStates[j].active = true; // mark frequency as already playing
					ym->channelSetAmpl(freqStates[j].ampl, i);
					break;
				}
			}
			if (synthStates[i].active == false) {
				synthStates[i].freq = 0;
				ym->channelKeyOff(i);
			}
		}
	}

	// Now find frequencies that are not active yet.
	for (i = 0; i < freqsUsed; i++) {
		if (freqStates[i].active == false) { // assign frequencies which are not playing yet to channels
			for (j = 0; j < CHANNELS; j++) {
				if (synthStates[j].active == false) {
					synthStates[j].active = true;
					synthStates[j].freq = freqStates[i].freq;
					freqStates[i].active = true;
					ym->channelSetAmpl(freqStates[i].ampl, j);
					ym->channelSetFreqAndKeyOn(freqStates[i].freq, j);
					break;
				}
			}
		}
	}
}

