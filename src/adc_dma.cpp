#include <adc_dma.h>
#include <Arduino.h>
#include <helper.h>
#include <utility/pdb.h>

DMAMEM static uint16_t dmaBuffer[FFT_SHIFT_LEN];
DMAChannel adc_dma::dma(false);

void adc_dma::init(uint8_t pin)
{
	// Configure the ADC and run at least one software-triggered conversion.
	analogReadRes(16);
	analogReference(INTERNAL1V2); // range 0 to 1.2 volts
	analogReadAveraging(4);
    analogRead(pin);

	// set the programmable delay block to trigger the ADC at defined rate
	SIM_SCGC6 |= SIM_SCGC6_PDB; // enable Programmable Delay Block clock gate
	PDB0_IDLY = 1; // set Interrupt Delay register
	PDB0_MOD = (uint32_t) (F_BUS / SAMP_RATE - 1); // modulus register
	PDB0_SC = PDB_CONFIG | PDB_SC_LDOK; // PDB0 Status and Control Register - load OK
	PDB0_SC = PDB_CONFIG | PDB_SC_SWTRIG; // software trigger
	PDB0_CH0C1 = 0x0101; // Channel 0 Control Register 1 - pretrigger?

	// enable ADC0 for hardware trigger and DMA
	ADC0_SC2 |= ADC_SC2_ADTRG | ADC_SC2_DMAEN;

	// set up a DMA channel to store the ADC data
	dma.begin(true); // force initialization
	dma.TCD->SADDR = &ADC0_RA; // DMA source = ADC0 result register
	dma.TCD->SOFF = 0;
	dma.TCD->ATTR = DMA_TCD_ATTR_SSIZE(1) | DMA_TCD_ATTR_DSIZE(1);
	dma.TCD->NBYTES_MLNO = 2; // 2 bytes per transfer
	dma.TCD->SLAST = 0;
	dma.TCD->DADDR = dmaBuffer; // DMA destination
	dma.TCD->DOFF = 2;
	dma.TCD->CITER_ELINKNO = FFT_SHIFT_LEN;
	dma.TCD->DLASTSGA = -(FFT_SHIFT_LEN * 2);
	dma.TCD->BITER_ELINKNO = FFT_SHIFT_LEN;
	dma.TCD->CSR = DMA_TCD_CSR_INTMAJOR;
	dma.triggerAtHardwareEvent(DMAMUX_SOURCE_ADC0);
	dma.enable();
	dma.attachInterrupt(isr);
}

void adc_dma::isr(void)
{
	dma.clearInterrupt();
	dma.TCD->DADDR = dmaBuffer;
	processADCInput(dmaBuffer);
}
