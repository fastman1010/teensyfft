/*
 * arrayFunc.cpp
 *
 *  Created on: 25.03.2019
 */

#include <Arduino.h>
#include <arm_math.h>
#include <arrayFunc.h>
#include <helper.h>
#include <SD.h>

/**
 * Initializes a window in array w of size n.
 */
void initWindow(float32_t *w, uint32_t n) {
//	float32_t alpha = 0.16;
	float32_t a0 = 0.35875;
	float32_t a1 = 0.48829;
	float32_t a2 = 0.14128;
	float32_t a3 = 0.01168;
	for (uint32_t i = 0; i < n; i++) {
//		w[i] = powf(arm_sin_f32(M_PI * i / n), 2); // Hann
//		w[i] = 1.0; // rectangular
//		w[i] = (1 - alpha / 2) - (1 / 2) * arm_cos_f32(2 * M_PI * i / n) + (alpha / 2) * arm_cos_f32(4 * M_PI * i / n); // Blackman
		w[i] = a0 -
				a1 * arm_cos_f32(2 * M_PI * i / n) +
				a2 * arm_cos_f32(4 * M_PI * i / n) -
				a3 * arm_cos_f32(6 * M_PI * i / n); // Blackman-Harris
	}
}


/**
 * Converts an array of size n of real values to an array of complex values (Im=0).
 */
void padRealToCompl(float32_t *in, float32_t *out, uint32_t n) {
	for (uint32_t i = 0; i < n; i++) {
		out[(i << 1)] = in[i];
		out[((i << 1) | 1)] = 0;
	}
}

/**
 * The buffers are supposed to be structured as following:
 * [0] [__][__][__][__]
 * [1]     [__][__][__][__]
 * [2]         [__][__][__][__]
 * [3]             [__][__][__][__]
 * Data goes into the buffer #0 rightmost part and is then
 * shifted into the buffers below. Then FFT is done on buffer #0.
 * Buffer #1 becomes the #0, #2 -> #1 etc. Block size is FFT_SHIFT_LEN.
 */


/**
 * Initialize buffers by filling with zeroes.
 */
void bufferInit(float32_t (*buf)[FFT_SHIFT_LEN]) {
	for (uint8_t i = 0; i < FFT_SHIFTS; i++) {
		arm_fill_f32(0, buf[i], FFT_SHIFT_LEN);
	}
}


/**
 * Copy freshly read data to the working array
 */
void bufferCopyPart(float32_t *data, float32_t (*buf)[FFT_SHIFT_LEN], uint8_t bufCurrent)
{
	uint8_t i;
	for (i = 0; i < FFT_SHIFTS; i++) {
		arm_copy_f32(buf[(i + bufCurrent) % FFT_SHIFTS], &data[i * FFT_SHIFT_LEN], FFT_SHIFT_LEN);
	}
}
