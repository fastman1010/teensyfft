/*
 * ym3812.cpp
 *
 *  Created on: 02.04.2019
 */

#include <vector>
#include <Arduino.h>
#include <arm_math.h>
#include <helper.h>
#include <process.h>
#include <WString.h>
#include <ym3812.h>

#define STR_SIZE 128

YM3812::YM3812() {
	chipFrequency = (float32_t) PWM_FREQ;
	chipSampleRate = chipFrequency / 72.0;
	frequencyCount = 6;
	prepareSpectrum = false;
}

/**
 * Initializes pins connecting MCU to the chip.
 */
void YM3812::initPins() {
	// Configure pins as outputs.
	// Currently I avoid working directly with MCU registers to keep it simple.
	pinMode(YM_IC, 1);
	digitalWriteFast(YM_IC, 0);
	pinMode(YM_CS, 1);
	digitalWriteFast(YM_CS, 1);
	pinMode(YM_WR, 1);
	digitalWriteFast(YM_WR, 1);
	pinMode(YM_RD, 1);
	digitalWriteFast(YM_RD, 1);
	pinMode(YM_A0, 1);
	digitalWriteFast(YM_A0, 0);

	pinMode(2, 1);
	pinMode(14, 1);
	pinMode(7, 1);
	pinMode(8, 1);
	pinMode(6, 1);
	pinMode(20, 1);
	pinMode(21, 1);
	pinMode(5, 1);

	delay(10);
}

/**
 * Chip initialization.
 */
void YM3812::init() {
	initPins();
	// Reset
	digitalWriteFast(YM_IC, 0);
	delayMicroseconds(100);
	digitalWriteFast(YM_IC, 1);
	delayMicroseconds(10);
	// Initialize
	write(0x01, 1 << 5); // Wave select enable
	write(0x04, 0x00); // Disable timers

	setDefaults();
}

/**
 * Sends a byte to the data bus of the chip.
 */
void YM3812::writeByte(uint8_t data) {
	digitalWriteFast(YM_CS, 0); // select chip
	YM_PORT_DATA = data;
	delayMicroseconds(10);
	digitalWriteFast(YM_WR, 0); // allow writing
	delayMicroseconds(5);
	digitalWriteFast(YM_WR, 1);
	delayMicroseconds(5);
	digitalWriteFast(YM_CS, 1);
}

/**
 * Writes a byte to the specified register.
 */
void YM3812::write(uint8_t reg, uint8_t data) {
	digitalWriteFast(YM_A0, 0); // mode = write address
	writeByte(reg);
	digitalWriteFast(YM_A0, 1); // mode = write data
	writeByte(data);
	state[reg] = data;
}

/**
 * Set default parameters. Sine waveform.
 */
void YM3812::setDefaults() {
	cfg.op1am = 0;		// Operator 1 amplitude modulation
	cfg.op1vib = 0;	// Operator 1 vibrato
	cfg.op1eg = 1;		// Operator 1 non-percussive envelope?
	cfg.op1ksr = 0;	// Operator 1 key scaling
	cfg.op1mul = 1;	// Operator 1 frequency multiplier, 4 bits
	cfg.op2am = 0;		// Operator 2 amplitude modulation
	cfg.op2vib = 0;	// Operator 2 vibrato
	cfg.op2eg = 1;		// Operator 2 non-percussive envelope?
	cfg.op2ksr = 0;	// Operator 2 frequency scaling
	cfg.op2mul = 1; // Operator 2 frequency multiplier, 4 bits
	cfg.op1ksl = 0;	// Operator 1 key scaling level, 2 bits
	cfg.op1tl = 0;	// Operator 1 total level, 6 bits
	cfg.op2ksl = 0;	// Operator 2 key scaling level, 2 bits
	cfg.op2tl = 0;	// Operator 2 total level, 6 bits
	cfg.op1ar = 0;	// Operator 1 attack rate, 4 bits
	cfg.op1dr = 0;	// Operator 1 decay rate, 4 bits
	cfg.op2ar = 13;	// Operator 2 attack rate, 4 bits
	cfg.op2dr = 0;	// Operator 2 decay rate, 4 bits
	cfg.op1sl = 0;	// Operator 1 sustain level, 4 bits
	cfg.op1rr = 15;	// Operator 1 release time, 4 bits
	cfg.op2sl = 0;	// Operator 2 sustain level, 4 bits
	cfg.op2rr = 13;	// Operator 2 release time, 4 bits
	cfg.vib = 1;		// Vibrato depth: 0 = 7c, 1 = 14c
	cfg.amp = 1;		// AM depth: 0 = 1dB, 1 = 4.8dB
	cfg.fb = 0;		// FM feedback, 3 bits
	cfg.conn = 0;		// Operator connection: 0 = FM, 1 = additive
	cfg.op1ws = 0;	// Operator 1 wave select, 2 bits
	cfg.op2ws = 0;	// Operator 2 wave select, 2 bits
	updateOp1();
	updateOp2();
	updateCh();
}

/**
 * Updates synthesis parameters within the chip (operator 1)
 */
void YM3812::updateOp1() {
	uint8_t i, offset;
	for (i = 0; i < CHANNELS; i++) {
		offset = getChannelShift(i);
		write(0x20 + offset, ((cfg.op1am << 7) | (cfg.op1vib << 6) | (cfg.op1eg << 5) | (cfg.op1ksr << 4) | cfg.op1mul));
		write(0x40 + offset, ((cfg.op1ksl << 6) | cfg.op1tl));
		write(0x60 + offset, ((cfg.op1ar << 4) | cfg.op1dr));
		write(0x80 + offset, ((cfg.op1sl << 4) | cfg.op1rr));
		write(0xE0 + offset, cfg.op1ws);
	}
}


/**
 * Updates synthesis parameters within the chip (operator 2)
 */
void YM3812::updateOp2() {
	uint8_t i, offset;
	for (i = 0; i < CHANNELS; i++) {
		offset = getChannelShift(i);
		write(0x23 + offset, ((cfg.op2am << 7) | (cfg.op2vib << 6) | (cfg.op2eg << 5) | (cfg.op2ksr << 4) | cfg.op2mul));
		write(0x43 + offset, ((cfg.op2ksl << 6) | cfg.op2tl));
		write(0x63 + offset, ((cfg.op2ar << 4) | cfg.op2dr));
		write(0x83 + offset, ((cfg.op2sl << 4) | cfg.op2rr));
		write(0xE3 + offset, cfg.op2ws);
	}
}


/**
 * Updates chip settings
 */
void YM3812::updateCh() {
	uint8_t i;
	write(0xBD, ((cfg.amp << 7) | (cfg.vib << 6)));
	for (i = 0; i < CHANNELS; i++) {
		write(0xC0 + i, ((cfg.fb << 1) | cfg.conn));
	}
}


/**
 * Converts frequency to F-Number, which can be transmitted to the chip.
 */
uint16_t YM3812::freqToFNum(float32_t freq) {
	// f-num = freq * 2^(20 - block) / 49716
	// where 3579545 / 72 ~= 49716.
	// We might have other chip frequency so this is not a constant.
	uint32_t fnum;
	uint16_t result;
	uint8_t block = 0;

	fnum = freq * 1048576 / chipSampleRate; // 2^20
	// We have 10 bits for the F-Number and 3 bits for the block...
	while (fnum >= 1024 && block < 8) {
		fnum = fnum >> 1;
		block++;
	}
	result = fnum | (block << 10);

	return result;
}

/**
 * Because the registers corresponding to each channel are not addressed
 * sequentially, we need to transform channel number into shift value.
 */
uint8_t YM3812::getChannelShift(uint8_t i) {
	switch (i) {
		case 0:
		case 1:
		case 2:
			return i;
		case 3:
		case 4:
		case 5:
			return (i + 5);
		case 6:
		case 7:
		case 8:
			return (i + 10);
		default:
			return i;
	}
}


/**
 * Set channel level.
 */
void YM3812::channelSetAmpl(float32_t ampl, uint8_t chan) {
	chan = getChannelShift(chan);
	uint8_t attn, op1level, op2level;
	if (ampl >= amplMax) {
		attn = 0;
	} else {
		ampl = 20 * log10f(ampl / amplMax); // normalize, MAX_AMPL becomes 1 -> 0 dB
		if (isnan(ampl)) {
			attn = 63;
		} else {
			attn = (uint8_t) (ampl / (-0.75)); // attenuation is given in dB as a positive value
		}
	}
	op1level = cfg.op1tl + attn;
	if (op1level > 63) op1level = 63; // cannot be softer
	op2level = cfg.op2tl + attn;
	if (op2level > 63) op2level = 63; // cannot be softer

	if (cfg.conn == true) { // additive
		write(0x40 + chan, (state[0x40 + chan] & 0b11000000) | op1level);
		write(0x43 + chan, (state[0x43 + chan] & 0b11000000) | op2level);
	} else { // FM
		write(0x43 + chan, (state[0x43 + chan] & 0b11000000) | op2level);
	}
}

/**
 * Set channel frequency.
 */
void YM3812::channelSetFreq(float32_t freq, uint8_t chan) {
	uint16_t f = freqToFNum(freq);
	write(0xA0 + chan, f & 0xFF);
	write(0xB0 + chan, (state[0xB0 + chan] & 0b11100000) | (f >> 8));
}

/**
 * Set channel frequency + key on.
 */
void YM3812::channelSetFreqAndKeyOn(float32_t freq, uint8_t chan) {
	uint16_t f = freqToFNum(freq);
	write(0xA0 + chan, f & 0xFF);
	write(0xB0 + chan, (state[0xB0 + chan] & 0b11000000) | (f >> 8) | (1 << 5));
}

/**
 * "Key on" on specified channel.
 */
void YM3812::channelKeyOn(uint8_t chan) {
	chan += 0xB0;
	write(chan, state[chan] | (1 << 5));
}

/**
 * "Key off" on specified channel.
 */
void YM3812::channelKeyOff(uint8_t chan) {
	chan += 0xB0;
	write(chan, state[chan] & ~(1 << 5));
}

/**
 * Process a vector of strings which contains a command.
 */
String YM3812::processCommand(std::vector<String> cmd) {
	uint8_t val, size;
	float32_t valf;
	char response[STR_SIZE];
	size = cmd.size();
	if (size == 0) {
		return F("Connection active.\r\n");
	}
	else if (size == 1) {
		if (cmd[0].equals("reset")) {
			setDefaults();
			return F("Default settings set.\r\n");
		} else if (cmd[0].equals("ch")) {
			sprintf(response, "Chip: vib=%d amp=%d fb=%d conn=%d\r\n",
					cfg.vib, cfg.amp, cfg.fb, cfg.conn);
			return String(response);
		} else if (cmd[0].equals("op1")) {
			sprintf(response, "OP1: am=%d vib=%d eg=%d ksr=%d mul=%d ksl=%d\r\n"
					"tl=%d ar=%d dr=%d sl=%d rr=%d ws=%d\r\n",
					cfg.op1am, cfg.op1vib, cfg.op1eg, cfg.op1ksr, cfg.op1mul, cfg.op1ksl,
					cfg.op1tl, cfg.op1ar, cfg.op1dr, cfg.op1sl, cfg.op1rr, cfg.op1ws);
			return String(response);
		} else if (cmd[0].equals("op2")) {
			sprintf(response, "OP2: am=%d vib=%d eg=%d ksr=%d mul=%d ksl=%d\r\n"
					"tl=%d ar=%d dr=%d sl=%d rr=%d ws=%d\r\n",
					cfg.op2am, cfg.op2vib, cfg.op2eg, cfg.op2ksr, cfg.op2mul, cfg.op2ksl,
					cfg.op2tl, cfg.op2ar, cfg.op2dr, cfg.op2sl, cfg.op2rr, cfg.op2ws);
			return String(response);
		} else if (cmd[0].equals("min")) {
			sprintf(response, "amplMin=%1.8f\r\n", amplMin);
			return String(response);
		} else if (cmd[0].equals("max")) {
			sprintf(response, "amplMax=%1.8f\r\n", amplMax);
			return String(response);
		} else if (cmd[0].equals("spec")) {
			prepareSpectrum = true;
			return F("Preparing spectrum values.\r\n");
		}
	} else if (size == 2) {
		if (cmd[0].equals("poly")) {
			if (sscanf(cmd[1].c_str(), "%hhd", &val) < 1) {
				return F("Argument 2 must be an integer.\r\n");
			}
			frequencyCount = val;
			sprintf(response, "Set to %d channels.\r\n", val);
			return String(response);
		} else if (cmd[0].equals("min")) {
			if (sscanf(cmd[1].c_str(), "%f", &valf) < 1) {
				return F("Argument 2 must be a float.\r\n");
			}
			amplMin = valf;
			sprintf(response, "Set amplMin to %1.8f\r\n", amplMin);
			return String(response);
		} else if (cmd[0].equals("max")) {
			if (sscanf(cmd[1].c_str(), "%f", &valf) < 1) {
				return F("Argument 2 must be a float.\r\n");
			}
			amplMax = valf;
			sprintf(response, "Set amplMax to %1.8f\r\n", amplMax);
			return String(response);
		}
	} else if (size == 3) {
		if (sscanf(cmd[2].c_str(), "%hhd", &val) < 1) {
			return F("Argument 3 must be an integer.\r\n");
		}

		if (cmd[0].equals("ch")) {
			if (cmd[1].equals("vib")) {
				cfg.vib = val & 1;
			} else if (cmd[1].equals("amp")) {
				cfg.amp = val & 1;
			} else if (cmd[1].equals("fb")) {
				cfg.fb = val & 7;
			} else if (cmd[1].equals("conn")) {
				cfg.conn = val & 1;
			} else {
				return F("Argument 2 not recognized.\r\n");
			}
			updateOp1();
			updateOp2();
			updateCh();
		} else if (cmd[0].equals("op1")) {
			if (cmd[1].equals("am")) {
				cfg.op1am = val & 1;
			} else if (cmd[1].equals("vib")) {
				cfg.op1vib = val & 1;
			} else if (cmd[1].equals("eg")) {
				cfg.op1eg = val & 1;
			} else if (cmd[1].equals("ksr")) {
				cfg.op1ksr = val & 1;
			} else if (cmd[1].equals("mul")) {
				cfg.op1mul = val & 15;
			} else if (cmd[1].equals("ksl")) {
				cfg.op1ksl = val & 3;
			} else if (cmd[1].equals("tl")) {
				cfg.op1tl = val & 63;
			} else if (cmd[1].equals("ar")) {
				cfg.op1ar = val & 15;
			} else if (cmd[1].equals("dr")) {
				cfg.op1dr = val & 15;
			} else if (cmd[1].equals("sl")) {
				cfg.op1sl = val & 15;
			} else if (cmd[1].equals("rr")) {
				cfg.op1rr = val & 15;
			} else if (cmd[1].equals("ws")) {
				cfg.op1ws = val & 3;
			} else {
				return F("Argument 2 not recognized.\r\n");
			}
			updateOp1();
		} else if (cmd[0].equals("op2")) {
			if (cmd[1].equals("am")) {
				cfg.op2am = val & 1;
			} else if (cmd[1].equals("vib")) {
				cfg.op2vib = val & 1;
			} else if (cmd[1].equals("eg")) {
				cfg.op2eg = val & 1;
			} else if (cmd[1].equals("ksr")) {
				cfg.op2ksr = val & 1;
			} else if (cmd[1].equals("mul")) {
				cfg.op2mul = val & 15;
			} else if (cmd[1].equals("ksl")) {
				cfg.op2ksl = val & 3;
			} else if (cmd[1].equals("tl")) {
				cfg.op2tl = val & 63;
			} else if (cmd[1].equals("ar")) {
				cfg.op2ar = val & 15;
			} else if (cmd[1].equals("dr")) {
				cfg.op2dr = val & 15;
			} else if (cmd[1].equals("sl")) {
				cfg.op2sl = val & 15;
			} else if (cmd[1].equals("rr")) {
				cfg.op2rr = val & 15;
			} else if (cmd[1].equals("ws")) {
				cfg.op2ws = val & 3;
			} else {
				return F("Argument 2 not recognized.\r\n");
			}
			updateOp2();
		} else {
			return F("Argument 1 (op1, op2, ch) not recognized.\r\n");
		}
		// This part is only done if the function has not yet returned anything.
		return F("Command OK.\r\n");
	} else {
		return F("Invalid command size.\r\n");
	}
	return F("Command not recognized.\r\n");
}
