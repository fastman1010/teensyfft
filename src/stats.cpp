/*
 * stats.cpp
 *
 *  Created on: 04.04.2019
 */

#include <Arduino.h>
#include <arm_math.h>
#include <helper.h>

/**
 * Print FFT results (single-sided spectrum) into the serial console.
 */
void printSpectrum(arm_cfft_radix4_instance_f32 *fftInst, float32_t *res) {
	Serial.printf("size=%d;\nrate=%d;\nres=[", fftInst->fftLen / 2, SAMP_RATE);
	for (uint32_t i = 0; i < fftInst->fftLen / 2; i++) {
		Serial.printf("%.8f ", res[i]);
	}
	Serial.print("];\n");
}


/**
 * Print detected frequencies into the serial console.
 */
void printFreqs(channelState *freqs, uint8_t freqsUsed) {
	Serial.printf("freqs=[");
	for (uint8_t i = 0; i < freqsUsed; i++) {
		Serial.printf("%.4f ", freqs[i].freq);
	}
	Serial.print("];\n");
	Serial.printf("ampls=[");
	for (uint8_t i = 0; i < freqsUsed; i++) {
		Serial.printf("%.8f ", freqs[i].ampl);
	}
	Serial.print("];\n");
}
