/*
 * arrayFunc.h
 *
 *  Created on: 25.03.2019
 */

#ifndef SRC_ARRAYFUNC_H_
#define SRC_ARRAYFUNC_H_

#include <helper.h>

void initWindow(float32_t *w, uint32_t n);
void padRealToCompl(float32_t *in, float32_t *out, uint32_t n);
void bufferInit(float32_t (*buf)[FFT_SHIFT_LEN]);
void bufferCopyPart(float32_t *data, float32_t (*buf)[FFT_SHIFT_LEN], uint8_t bufCurrent);

#endif /* SRC_FFTCALC_H_ */
