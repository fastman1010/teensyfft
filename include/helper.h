/*
 * helper.h
 *
 *  Created on: 25.03.2019
 */

#ifndef SRC_HELPER_H_
#define SRC_HELPER_H_

#include <vector>
#include <Arduino.h>
#include <arm_math.h>
#include <WString.h>

#define FFT_LEN (uint16_t) (4096)
#define FFT_SHIFTS (uint8_t) (16)
#define FFT_SHIFT_LEN (uint16_t) (FFT_LEN / FFT_SHIFTS)
#define SAMP_RATE (uint16_t) (32000)
#define BIN_TO_FREQ (SAMP_RATE / FFT_LEN)
#define CHANNELS (uint8_t) (9)
#define PWM_PIN (uint8_t) (23)
#define PWM_FREQ (float32_t) (F_BUS / 16) // for Teensy 3.6 F_BUS = 60 MHz, therefore clock = 3.75 MHz
#define PWM_RES (uint8_t) (4)
#define MIN_FREQ (float32_t) (30)
#define MAX_DEV (float32_t) (0.015)

typedef struct {
	bool active = false;
	float32_t freq = 0;
	float32_t ampl = 0;
} channelState;

extern IntervalTimer ledTimer;
void keepAlive();
void systemError(char *msg);
void generateSineWave(float *wave, uint16_t freq, uint16_t rate, uint16_t samps);
void processADCInput(uint16_t *adcBuffer); // in main.cpp
std::vector<String> splitString(String cmd, uint8_t delimiter);
void splitString(std::vector<String> *cmdVec, String cmd, uint8_t delimiter);

#endif /* SRC_HELPER_H_ */
