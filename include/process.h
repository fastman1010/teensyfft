/*
 * process.h
 *
 *  Created on: 12.04.2019
 */

#ifndef INCLUDE_PROCESS_H_
#define INCLUDE_PROCESS_H_

#include <ym3812.h>

uint32_t findMaxValues(float32_t *specAbs, float32_t *specCompl, uint8_t numFreqs, channelState *freqStates);
void playFrequencies(YM3812 *ym, uint8_t freqsUsed, channelState *freqStates, channelState *voiceStates);
extern float32_t amplMin, amplMax;

#endif /* INCLUDE_PROCESS_H_ */
