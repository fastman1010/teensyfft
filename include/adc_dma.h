#ifndef INCLUDE_ADC_DMA_H_
#define INCLUDE_ADC_DMA_H_

#include "Arduino.h"
#include "DMAChannel.h"

class adc_dma {
public:
	adc_dma(uint8_t pin) { init(pin); }
	friend void dma_ch9_isr(void);
private:
	static DMAChannel dma;
	static void isr(void);
	static void init(uint8_t pin);
};

#endif
