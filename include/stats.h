/*
 * stats.h
 *
 *  Created on: 04.04.2019
 */

#ifndef SRC_STATS_H_
#define SRC_STATS_H_

#include <helper.h>

void printSpectrum(arm_cfft_radix4_instance_f32 *fftInst, float32_t *res);
void printFreqs(channelState *freqs, uint8_t freqsUsed);

#endif /* SRC_STATS_H_ */
