/*
 * btComm.h
 *
 *  Created on: 22.04.2019
 */

#ifndef SRC_BTCOMM_H_
#define SRC_BTCOMM_H_

#define BT_PIN_STATE (uint8_t) (18)
#define BT_SER Serial1 // change to actual port

class BTComm {
public:
	BTComm();
	void init();
	bool isConnected();
	String readCommand();
	void writeString(String data);
	void end();
	virtual ~BTComm();
};

#endif /* SRC_BTCOMM_H_ */
