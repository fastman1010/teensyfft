/*
 * ym3812.h
 *
 *  Created on: 02.04.2019
 */

#ifndef SRC_YM3812_H_
#define SRC_YM3812_H_

#define YM_CS (27)
#define YM_RD (28)
#define YM_WR (29)
#define YM_A0 (30)
#define YM_IC (31)
#define YM_PORT_DIR  GPIOD_PDDR
#define YM_PORT_DATA GPIOD_PDOR

class YM3812 {
	typedef struct {
		// 0x20
		uint8_t op1am;	// Operator 1 amplitude modulation, 1 bit
		uint8_t op1vib;	// Operator 1 vibrato, 1 bit
		uint8_t op1eg;	// Operator 1 percussive envelope, 1 bit
		uint8_t op1ksr;	// Operator 1 key scaling, 1 bit
		uint8_t op1mul;	// Operator 1 frequency multiplier, 4 bits
		// 0x23
		uint8_t op2am;	// Operator 2 amplitude modulation, 1 bit
		uint8_t op2vib;	// Operator 2 vibrato, 1 bit
		uint8_t op2eg;	// Operator 2 percussive envelope, 1 bit
		uint8_t op2ksr;	// Operator 2 frequency scaling, 1 bit
		uint8_t op2mul; // Operator 2 frequency multiplier, 4 bits
		// 0x40
		uint8_t op1ksl;	// Operator 1 key scaling level, 2 bits
		uint8_t op1tl;	// Operator 1 total level, 6 bits
		// 0x43
		uint8_t op2ksl;	// Operator 2 key scaling level, 2 bits
		uint8_t op2tl;	// Operator 2 total level, 6 bits
		// 0x60
		uint8_t op1ar;	// Operator 1 attack rate, 4 bits
		uint8_t op1dr;	// Operator 1 decay rate, 4 bits
		// 0x63
		uint8_t op2ar;	// Operator 2 attack rate, 4 bits
		uint8_t op2dr;	// Operator 2 decay rate, 4 bits
		// 0x80
		uint8_t op1sl;	// Operator 1 sustain level, 4 bits
		uint8_t op1rr;	// Operator 1 release time, 4 bits
		// 0x83
		uint8_t op2sl;	// Operator 2 sustain level, 4 bits
		uint8_t op2rr;	// Operator 2 release time, 4 bits
		// 0xBD
		uint8_t vib;	// Vibrato depth: 0 = 7c, 1 = 14c
		uint8_t amp;	// AM depth: 0 = 1dB, 1 = 4.8dB
		// 0xC0
		uint8_t fb;		// FM feedback, 3 bits
		uint8_t conn;	// Operator connection: 0 = FM, 1 = AM
		// 0xE0
		uint8_t op1ws;	// Operator 1 wave select, 2 bits
		// 0xE3
		uint8_t op2ws;	// Operator 2 wave select, 2 bits
	} ymConfig;
public:
	YM3812();
	void init();
	void write(uint8_t reg, uint8_t data);
	void channelSetFreq(float32_t freq, uint8_t voice);
	void channelSetAmpl(float32_t ampl, uint8_t voice);
	void channelSetFreqAndKeyOn(float32_t freq, uint8_t voice);
	void channelKeyOn(uint8_t voice);
	void channelKeyOff(uint8_t voice);
	String processCommand(std::vector<String> cmd);
	uint16_t freqToFNum(float32_t freq);
	uint8_t frequencyCount;
	bool prepareSpectrum;
private:
	void writeByte(uint8_t data);
	void initPins();
	void setDefaults();
	void updateOp1();
	void updateOp2();
	void updateCh();
	float32_t chipSampleRate;
	float32_t chipFrequency;
	uint8_t getChannelShift(uint8_t i);
	uint8_t state[244];
	ymConfig cfg;
};

#endif /* SRC_YM3812_H_ */
